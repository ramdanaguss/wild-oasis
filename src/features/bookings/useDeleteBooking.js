import { useMutation, useQueryClient } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { deleteBookingApi } from '../../services/apiBookings';

export function useDeleteBooking() {
  const queryClient = useQueryClient();

  const { mutate: deleteBooking, isDeleting } = useMutation({
    mutationFn(id) {
      return deleteBookingApi(id);
    },
    onSuccess() {
      toast.success('Successfully delete a booking');
      queryClient.invalidateQueries({
        queryKey: ['bookings'],
      });
    },
    onError(error) {
      toast.error(error.message);
      console.error(error);
    },
  });

  return { deleteBooking, isDeleting };
}
