import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

import { getBookingApi } from '../../services/apiBookings';

export function useBooking() {
  const { bookingId } = useParams();

  const { data: booking, isPending: isFetching } = useQuery({
    queryKey: ['booking', bookingId],
    queryFn: () => getBookingApi(bookingId),
  });

  return { booking, isFetching };
}
