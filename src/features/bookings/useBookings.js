import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useSearchParams } from 'react-router-dom';

import { getBookingsApi } from '../../services/apiBookings';
import { usePagination } from '../../hooks/usePagination';

export function useBookings() {
  const [searchParams] = useSearchParams();
  const queryClient = useQueryClient();

  const filterValue = searchParams.get('status') || 'all';
  const filter =
    filterValue === 'all' ? null : { field: 'status', value: filterValue, method: 'eq' };

  const sortByValue = searchParams.get('sortBy') || 'startDate-desc';
  const [sortField, sortDirection] = sortByValue.split('-');
  const sortBy = { field: sortField, direction: sortDirection };

  const { fromAPI: from, toAPI: to, page } = usePagination();
  const pageOption = { from, to };

  const { data: { bookings, count } = {}, isPending: isFetching } = useQuery({
    queryKey: ['bookings', filter, sortBy, page],
    queryFn: () => getBookingsApi({ filter, sortBy, pageOption }),
  });

  const {
    isFirstPage,
    isLastPage,
    prevPage,
    prevFromApi,
    prevToAPI,
    nextPage,
    nextFromAPI,
    nextToAPI,
  } = usePagination(count);

  if (!isFirstPage) {
    const pageOption = { from: prevFromApi, to: prevToAPI };

    queryClient.prefetchQuery({
      queryKey: ['bookings', filter, sortBy, prevPage],
      queryFn: () => getBookingsApi({ filter, sortBy, pageOption }),
    });
  }

  if (!isLastPage) {
    const pageOption = { from: nextFromAPI, to: nextToAPI };

    queryClient.prefetchQuery({
      queryKey: ['bookings', filter, sortBy, nextPage],
      queryFn: () => getBookingsApi({ filter, sortBy, pageOption }),
    });
  }

  return { bookings, count, isFetching };
}
