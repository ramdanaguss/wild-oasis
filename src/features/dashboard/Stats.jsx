import {
  HiOutlineBriefcase,
  HiOutlineBanknotes,
  HiOutlineCalendarDays,
  HiOutlineChartBar,
} from 'react-icons/hi2';

import Stat from './Stat';
import { formatCurrency } from '../../utils/helper';

export default function Stats({ bookings, confirmedStays, cabinCount, numDays }) {
  const numOfBookings = bookings.length;

  const totalSales = bookings
    .filter((booking) => booking.isPaid)
    .reduce((totalSales, booking) => totalSales + booking.totalPrice, 0);

  const checkIns = confirmedStays.filter(
    (booking) => booking.status === 'checked-in'
  ).length;

  const totalGuestsNight = confirmedStays.reduce(
    (totalNight, booking) => totalNight + booking.numNights,
    0
  );
  const maxPossibleNights = cabinCount * numDays;
  const occupancyRate = `${Math.round((totalGuestsNight / maxPossibleNights) * 100)}%`;

  return (
    <>
      <Stat
        title="Bookings"
        color="blue"
        icon={<HiOutlineBriefcase />}
        value={numOfBookings}
      />

      <Stat
        title="Sales"
        color="green"
        icon={<HiOutlineBanknotes />}
        value={formatCurrency(totalSales)}
      />

      <Stat
        title="Check ins"
        color="indigo"
        icon={<HiOutlineCalendarDays />}
        value={checkIns}
      />

      <Stat
        title="Occupancy rate"
        color="yellow"
        icon={<HiOutlineChartBar />}
        value={occupancyRate}
      />
    </>
  );
}
