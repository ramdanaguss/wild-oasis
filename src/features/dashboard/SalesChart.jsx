import styled from 'styled-components';
import { eachDayOfInterval, format, isSameDay, subDays } from 'date-fns';
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';

import DashboardBox from './DashboardBox';
import Heading from '../../ui/Heading';

import { useDarkMode } from '../../context/DarkModeContext';

const StyledSalesChart = styled(DashboardBox)`
  grid-column: 1 / -1;

  /* recharts grid line colors */
  & .recharts-cartesian-grid-horizontal line,
  & .recharts-cartesian-grid-vertical line {
    stroke: var(--color-grey-300);
  }
`;

function SalesChart({ numOfDays, bookings }) {
  const { isDarkMode } = useDarkMode();

  const targetDate = subDays(new Date(), numOfDays);
  const dateInterval = eachDayOfInterval({ start: targetDate, end: new Date() });

  const paidBookings = bookings.filter((booking) => booking.isPaid);

  const salesData = dateInterval.map((date) => {
    const totalSales = paidBookings
      .filter((booking) => isSameDay(new Date(booking.created_at), date))
      .reduce((totalExtrasSales, booking) => totalExtrasSales + booking.totalPrice, 0);

    const extrasSales = paidBookings
      .filter((booking) => isSameDay(new Date(booking.created_at), date))
      .reduce((totalExtrasSales, booking) => totalExtrasSales + booking.extrasPrice, 0);

    return {
      label: format(date, 'MMM dd'),
      totalSales,
      extrasSales,
    };
  });

  const colors = isDarkMode
    ? {
        totalSales: { stroke: '#4f46e5', fill: '#4f46e5' },
        extrasSales: { stroke: '#22c55e', fill: '#22c55e' },
        text: '#e5e7eb',
        background: '#18212f',
      }
    : {
        totalSales: { stroke: '#4f46e5', fill: '#c7d2fe' },
        extrasSales: { stroke: '#16a34a', fill: '#dcfce7' },
        text: '#374151',
        background: '#fff',
      };

  return (
    <StyledSalesChart>
      <Heading as="h2">
        Sales from {format(dateInterval.at(0), 'MMM dd yyyy')} &mdash;{' '}
        {format(dateInterval.at(-1), 'MMM dd yyyy')}
      </Heading>

      <ResponsiveContainer height={300} width="100%">
        <AreaChart data={salesData} margin="20%">
          <Tooltip contentStyle={{ backgroundColor: colors.background }} />
          <CartesianGrid strokeDasharray={4} />
          <XAxis
            dataKey="label"
            tick={{ fill: colors.text, fontSize: '10px' }}
            tickLine={{ stroke: colors.text }}
          />
          <YAxis
            unit=" Rupiah"
            tick={{ fill: colors.text, fontSize: '10px' }}
            tickLine={{ stroke: colors.text }}
            width={150}
          />
          <Area
            dataKey="totalSales"
            type="monotone"
            stroke={colors.totalSales.stroke}
            fill={colors.totalSales.fill}
            strokeWidth={2}
            unit=" Rupiah"
            name="Total Sales"
          />
          <Area
            dataKey="extrasSales"
            type="monotone"
            stroke={colors.extrasSales.stroke}
            fill={colors.extrasSales.fill}
            strokeWidth={2}
            unit=" Rupiah"
            name="Extra Sales"
          />
        </AreaChart>
      </ResponsiveContainer>
    </StyledSalesChart>
  );
}

export default SalesChart;
