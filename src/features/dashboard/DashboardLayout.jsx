import styled from 'styled-components';

import Spinner from '../../ui/Spinner';
import Stats from './Stats';
import SalesChart from './SalesChart';
import DurationChart from './DurationChart';
import TodayActivity from '../check-in-out/TodayActivity';

import { useRecentBooking } from './useRecentBookings';
import { useRecentStays } from './useRecentStays';
import { useCabins } from '../cabins/useCabins';

const StyledDashboardLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: auto 34rem auto;
  gap: 2.4rem;
`;

function DashboardLayout() {
  const { bookings, isFetching: isFetchingRecentBookings } = useRecentBooking();
  const {
    confirmedStays,
    isFetching: isFetchingRecentStays,
    numOfDays,
  } = useRecentStays();
  const { count } = useCabins();

  if (isFetchingRecentBookings || isFetchingRecentStays) return <Spinner />;

  return (
    <StyledDashboardLayout>
      <Stats
        bookings={bookings}
        confirmedStays={confirmedStays}
        cabinCount={count}
        numDays={numOfDays}
      />
      <TodayActivity />
      <DurationChart confirmedStays={confirmedStays} />
      <SalesChart numOfDays={numOfDays} bookings={bookings} />
    </StyledDashboardLayout>
  );
}

export default DashboardLayout;
