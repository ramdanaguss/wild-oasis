import { useQuery } from '@tanstack/react-query';
import { useSearchParams } from 'react-router-dom';
import { getBookingsAfterDateApi } from '../../services/apiBookings';
import { subDays } from 'date-fns';

export function useRecentBooking() {
  const [searchParams] = useSearchParams();
  const numOfDays = +searchParams.get('last') || 7;
  const targetedDateString = subDays(new Date(), numOfDays).toISOString();

  const { data: bookings, isFetching } = useQuery({
    queryKey: ['bookings', `last-${numOfDays}-bookings`],
    queryFn: () => getBookingsAfterDateApi(targetedDateString),
  });

  return { bookings, isFetching };
}
