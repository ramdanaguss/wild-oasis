import { useQuery } from '@tanstack/react-query';
import { useSearchParams } from 'react-router-dom';
import { getStaysAfterDateApi } from '../../services/apiBookings';
import { subDays } from 'date-fns';

export function useRecentStays() {
  const [searchParams] = useSearchParams();
  const numOfDays = +searchParams.get('last') || 7;
  const targetedDateString = subDays(new Date(), numOfDays).toISOString();

  const { data: stays, isPending: isFetching } = useQuery({
    queryKey: ['bookings', `last-${numOfDays}-stays`],
    queryFn() {
      return getStaysAfterDateApi(targetedDateString);
    },
  });

  const confirmedStays = stays?.filter(
    (stay) => stay.status === 'checked-in' || stay.status === 'checked-out'
  );

  return { confirmedStays, isFetching, numOfDays };
}
