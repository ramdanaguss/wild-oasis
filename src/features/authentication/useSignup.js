import { useMutation } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { signUpApi } from '../../services/apiAuthentication';

export function useSignup() {
  const { mutate: signup, isPending: isSigningUp } = useMutation({
    mutationFn({ email, password, fullName }) {
      return signUpApi({ email, password, fullName });
    },
    onSuccess(data) {
      toast.success(
        `Account successfully created! Please verify the new account from ${data.user.email}`
      );
    },
    onError(error) {
      toast.error(error.message);
      console.error(error);
    },
  });
  return { signup, isSigningUp };
}
