import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';

import Button from '../../ui/Button';
import Form from '../../ui/Form';
import FormRow from '../../ui/FormRow';
import Input from '../../ui/Input';
import { useSignup } from './useSignup';

function SignupForm() {
  const { signup, isSigningUp } = useSignup();

  const { register, formState, handleSubmit, reset } = useForm({
    defaultValues: {
      fullName: null,
      email: null,
      password: null,
      passwordConfirm: null,
    },
  });

  const { errors } = formState;
  const onSubmit = handleSubmit(onValidInput, onInvalidInput);

  function onValidInput({ email, password, fullName }) {
    signup(
      { email, password, fullName },
      {
        onSettled() {
          reset();
        },
      }
    );
  }

  function onInvalidInput() {
    toast.error('Please provide all valid user data');
  }

  return (
    <Form onSubmit={onSubmit}>
      <FormRow label="Full name" errorMessage={errors?.fullName?.message}>
        <Input
          type="text"
          id="fullName"
          disabled={isSigningUp}
          {...register('fullName', { required: 'Full name is required' })}
        />
      </FormRow>

      <FormRow label="Email address" errorMessage={errors?.email?.message}>
        <Input
          type="email"
          id="email"
          disabled={isSigningUp}
          {...register('email', {
            required: 'Email address is required',
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: 'Please provide valid email address',
            },
          })}
        />
      </FormRow>

      <FormRow
        label="Password (min 8 characters)"
        errorMessage={errors?.password?.message}
      >
        <Input
          type="password"
          id="password"
          disabled={isSigningUp}
          {...register('password', {
            required: 'Password is required',
            minLength: {
              value: 8,
              message: 'Password needs a minimum of 8 characters',
            },
          })}
        />
      </FormRow>

      <FormRow label="Repeat password" errorMessage={errors?.passwordConfirm?.message}>
        <Input
          type="password"
          id="passwordConfirm"
          disabled={isSigningUp}
          {...register('passwordConfirm', {
            required: 'Repeat password is required',
            validate(value, formValues) {
              return value === formValues.password || 'Password is not repeated';
            },
          })}
        />
      </FormRow>

      <FormRow>
        <Button $variation="secondary" type="reset" onClick={reset}>
          Cancel
        </Button>
        <Button disabled={isSigningUp}>Create new user</Button>
      </FormRow>
    </Form>
  );
}

export default SignupForm;
