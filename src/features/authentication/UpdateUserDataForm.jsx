import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';

import Button from '../../ui/Button';
import FileInput from '../../ui/FileInput';
import Form from '../../ui/Form';
import FormRow from '../../ui/FormRow';
import Input from '../../ui/Input';

import { useUpdateUser } from './useUpdateUser';
import { useUser } from './useUser';

function UpdateUserDataForm() {
  const {
    user: {
      email,
      user_metadata: { fullName: currentFullName, avatar },
    },
  } = useUser();

  const { updateUser, isUpdating } = useUpdateUser();

  const { register, handleSubmit, formState, setValue } = useForm({
    defaultValues: {
      fullName: currentFullName,
      avatar,
    },
  });

  const { errors } = formState;
  const onSubmit = handleSubmit(onValidInput, onInvalidInput);

  function onValidInput({ fullName, avatar }) {
    updateUser({ fullName, avatar });
  }

  function onInvalidInput() {
    toast.error('Please provide all valid user data');
  }

  function resetToInitial() {
    setValue('fullName', currentFullName);
    setValue('avatar', avatar);
  }

  return (
    <Form onSubmit={onSubmit}>
      <FormRow label="Email address">
        <Input value={email} disabled />
      </FormRow>

      <FormRow label="Full name" errorMessage={errors?.fullName?.message}>
        <Input
          type="text"
          id="fullName"
          disabled={isUpdating}
          {...register('fullName', { required: 'Full name is required' })}
        />
      </FormRow>

      <FormRow label="Avatar image" errorMessage={errors?.avatar?.message}>
        <FileInput
          id="avatar"
          accept="image/*"
          disabled={isUpdating}
          {...register('avatar')}
        />
      </FormRow>

      <FormRow>
        <Button type="button" $variation="secondary" onClick={resetToInitial}>
          Cancel
        </Button>
        <Button>Update account</Button>
      </FormRow>
    </Form>
  );
}

export default UpdateUserDataForm;
