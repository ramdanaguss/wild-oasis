import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import toast from 'react-hot-toast';

import { logoutApi } from '../../services/apiAuthentication';

export function useLogout() {
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const { mutate: logout, isPending: isLogingOut } = useMutation({
    mutationFn: logoutApi,
    onSuccess() {
      queryClient.removeQueries();
      navigate('/login');
    },
    onError(error) {
      toast.error('Cannot logout for now. Please try again later');
      console.error(error);
    },
  });

  return { logout, isLogingOut };
}
