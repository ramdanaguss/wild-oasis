import { useQuery } from '@tanstack/react-query';
import { getUserApi } from '../../services/apiAuthentication';

export function useUser() {
  const { data: user, isPending: isFetchingUser } = useQuery({
    queryKey: ['user'],
    queryFn: getUserApi,
  });

  return { user, isFetchingUser, isAuthenticated: user?.role === 'authenticated' };
}
