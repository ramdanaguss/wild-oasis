import { useMutation, useQueryClient } from '@tanstack/react-query';
import { loginApi } from '../../services/apiAuthentication';
import toast from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';

export function useLogin() {
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const { mutate: login, isPending: isLogingIn } = useMutation({
    mutationFn({ email, password }) {
      return loginApi(email, password);
    },
    onSuccess(data) {
      queryClient.setQueryData(['user'], data.user);
      navigate('/dashboard', { replace: true });
    },
    onError(error) {
      toast.error('Provided email or password are incorrect');
      console.error(error);
    },
  });

  return { login, isLogingIn };
}
