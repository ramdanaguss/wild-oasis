import { useMutation, useQueryClient } from '@tanstack/react-query';
import {
  updateUserDataApi,
  updateUserPasswordApi,
} from '../../services/apiAuthentication';
import toast from 'react-hot-toast';

export function useUpdateUser(type = 'user-data') {
  const queryClient = useQueryClient();

  const mutationFn = type === 'user-data' ? updateUserDataApi : updateUserPasswordApi;

  const { mutate: updateUser, isPending: isUpdating } = useMutation({
    mutationFn,
    onSuccess() {
      toast.success('Successfully update user');
      queryClient.invalidateQueries({ queryKey: ['user'] });
    },
    onError(error) {
      console.error(error);
      toast.error(error.message);
    },
  });

  return { updateUser, isUpdating };
}
