import { useMutation, useQueryClient } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { updateSettingApi } from '../../services/apiSettings';

export function useUpdateSetting() {
  const queryClient = useQueryClient();

  const { mutate: updateSetting, isPending: isUpdating } = useMutation({
    mutationFn: updateSettingApi,
    onSuccess() {
      toast.success('Successfully update setting');
      queryClient.invalidateQueries();
    },
    onError(error) {
      console.error(error);
      toast.error(error.message);
    },
  });

  return { updateSetting, isUpdating };
}
