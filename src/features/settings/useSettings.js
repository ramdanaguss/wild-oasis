import { useQuery } from '@tanstack/react-query';
import { getSettingsApi } from '../../services/apiSettings';

export function useSettings() {
  const { data: settings, isPending: isFetchingSetting } = useQuery({
    queryFn: getSettingsApi,
    queryKey: ['settings'],
  });

  return { settings, isFetchingSetting };
}
