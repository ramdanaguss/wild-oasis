import Filter from '../../ui/Filter';
import SortBy from '../../ui/SortBy';
import TableOperations from '../../ui/TableOperations';

const filterOptions = [
  { label: 'All', value: 'all' },
  { label: 'With Discount', value: 'with-discount' },
  { label: 'No Discount', value: 'no-discount' },
];

const sortOptions = [
  { label: 'Sort by name (A-Z)', value: 'name-asc' },
  { label: 'Sort by name (Z-A)', value: 'name-dsc' },
  { label: 'Sort by price (low first)', value: 'regularPrice-asc' },
  { label: 'Sort by price (high first)', value: 'regularPrice-dsc' },
  { label: 'Sort by capacity (low first)', value: 'maxCapacity-asc' },
  { label: 'Sort by capacity (high first)', value: 'maxCapacity-dsc' },
];

function CabinOperations() {
  return (
    <TableOperations>
      <Filter filterField="discount" options={filterOptions} />
      <SortBy options={sortOptions} />
    </TableOperations>
  );
}

export default CabinOperations;
