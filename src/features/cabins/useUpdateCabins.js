import { useMutation, useQueryClient } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { updateCabinApi } from '../../services/apiCabins';

export function useUpdateCabins() {
  const queryClient = useQueryClient();

  const { mutate: updateCabin, isPending: isUpdating } = useMutation({
    mutationFn({ data, id }) {
      return updateCabinApi(data, id);
    },
    onSuccess() {
      toast.success('Successfully update cabin');
      queryClient.invalidateQueries({ queryKey: ['cabins'] });
    },
    onError(error) {
      console.error(error);
      toast.error(error.message);
    },
  });

  return { updateCabin, isUpdating };
}
