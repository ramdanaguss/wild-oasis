import Modal from '../../ui/Modal';
import Button from '../../ui/Button';
import CreateCabinForm from './CreateCabinForm';

function AddCabin() {
  return (
    <div>
      <Modal>
        <Modal.Open target="cabin-add-form">
          <Button $variation="primary" $size="large">
            Add Cabin
          </Button>
        </Modal.Open>

        <Modal.Window name="cabin-add-form">
          <CreateCabinForm />
        </Modal.Window>
      </Modal>
    </div>
  );
}

export default AddCabin;
