import { useQuery } from '@tanstack/react-query';
import { getCabinsApi } from '../../services/apiCabins';

export function useCabins() {
  const { data: { cabins, count } = {}, isPending: isFetchingCabin } = useQuery({
    queryKey: ['cabins'],
    queryFn: getCabinsApi,
  });

  return { cabins, count, isFetchingCabin };
}
