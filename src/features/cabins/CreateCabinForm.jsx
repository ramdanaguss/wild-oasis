import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';

import Input from '../../ui/Input';
import Form from '../../ui/Form';
import Button from '../../ui/Button';
import Textarea from '../../ui/Textarea';
import FormRow from '../../ui/FormRow';
import FileInput from '../../ui/FileInput';

import { useCreateCabins } from './useCreateCabins';
import { useUpdateCabins } from './useUpdateCabins';

function CreateCabinForm({ cabinToUpdate = {}, onCloseModal = () => {} }) {
  const { id, ...cabin } = cabinToUpdate;
  const isUpdateSession = !!id;
  const isModal = !!onCloseModal;

  const { register, handleSubmit, reset, formState } = useForm({
    defaultValues: isUpdateSession
      ? cabin
      : {
          name: null,
          maxCapacity: null,
          regularPrice: null,
          discount: null,
          description: null,
          image: null,
        },
  });

  const { createCabin, isCreating } = useCreateCabins();
  const { updateCabin, isUpdating } = useUpdateCabins();

  const isWorking = isCreating || isUpdating;

  const { errors } = formState;
  const onSubmit = handleSubmit(onValidInput, onInvalidInput);

  function onValidInput(data) {
    if (isUpdateSession) {
      updateCabin(
        { data, id },
        {
          onSuccess() {
            onCloseModal();
          },
        }
      );
    } else {
      createCabin(data, {
        onSuccess() {
          reset();
          onCloseModal();
        },
      });
    }
  }

  function onInvalidInput() {
    toast.error('Please provide all valid cabin data');
  }

  return (
    <Form onSubmit={onSubmit} $type={isModal ? 'modal' : 'regular'}>
      <FormRow label="Cabin name" errorMessage={errors?.name?.message}>
        <Input
          type="text"
          id="name"
          disabled={isWorking}
          {...register('name', {
            required: 'Cabin name must be filled',
          })}
        />
      </FormRow>

      <FormRow label="Maximum capacity" errorMessage={errors?.maxCapacity?.message}>
        <Input
          type="number"
          id="maxCapacity"
          disabled={isWorking}
          {...register('maxCapacity', {
            required: 'Maximum capacity must be filled',
            min: {
              value: 1,
              message: 'Maximum capacity at least must be 1',
            },
          })}
        />
      </FormRow>

      <FormRow label="Regular price" errorMessage={errors?.regularPrice?.message}>
        <Input
          type="number"
          id="regularPrice"
          disabled={isWorking}
          {...register('regularPrice', {
            required: 'Regular price must be filled',
            min: {
              value: 1,
              message: 'Regular price must be greter than 0',
            },
          })}
        />
      </FormRow>

      <FormRow label="Discount" errorMessage={errors?.discount?.message}>
        <Input
          type="number"
          id="discount"
          disabled={isWorking}
          {...register('discount', {
            required: 'Discount must be filled',
            validate(value, formValues) {
              return (
                +value < +formValues.regularPrice ||
                'Discount must be less than regular price'
              );
            },
          })}
        />
      </FormRow>

      <FormRow
        label="Description for website"
        errorMessage={errors?.description?.message}
      >
        <Textarea
          id="description"
          defaultValue=""
          disabled={isWorking}
          {...register('description', {
            required: 'Description for website must be filled',
          })}
        />
      </FormRow>

      <FormRow label="Cabin photo" errorMessage={errors?.image?.message}>
        <FileInput
          id="image"
          accept="image/*"
          disabled={isWorking}
          {...register('image', {
            required: isUpdateSession ? false : 'Image must be filled',
          })}
        />
      </FormRow>

      <FormRow>
        <Button
          onClick={onCloseModal}
          $size="medium"
          $variation="secondary"
          type="reset"
          disabled={isWorking}
        >
          Cancel
        </Button>
        <Button $size="medium" $variation="primary" disabled={isWorking}>
          {isUpdateSession ? 'Update cabin' : 'Add cabin'}
        </Button>
      </FormRow>
    </Form>
  );
}

export default CreateCabinForm;
