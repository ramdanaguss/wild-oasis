import { useMutation, useQueryClient } from '@tanstack/react-query';
import { deleteCabinApi } from '../../services/apiCabins';
import toast from 'react-hot-toast';

export function useDeleteCabin() {
  const queryClient = useQueryClient();

  const { mutate: deleteCabin, isPending: isDeleting } = useMutation({
    mutationFn: deleteCabinApi,
    onSuccess() {
      toast.success('Successfully delete a cabin');
      queryClient.invalidateQueries({
        queryKey: ['cabins'],
      });
    },
    onError(error) {
      toast.error(error.message);
      console.error(error);
    },
  });

  return { deleteCabin, isDeleting };
}
