import { useSearchParams } from 'react-router-dom';
import Menus from '../../ui/Menus';
import Spinner from '../../ui/Spinner';
import Table from '../../ui/Table';
import CabinRow from './CabinRow';

import { useCabins } from './useCabins';

function CabinTable() {
  const { cabins, isFetchingCabin } = useCabins();
  const [searchParams] = useSearchParams();

  let modifiedCabins;

  // 1) Filter
  const filterValue = searchParams.get('discount') || 'all';

  if (filterValue === 'all') modifiedCabins = cabins || [];
  if (filterValue === 'with-discount')
    modifiedCabins = cabins.filter((cabin) => cabin.discount > 0);
  if (filterValue === 'no-discount')
    modifiedCabins = cabins.filter((cabin) => !cabin.discount);

  // 2) Sort
  const sortByValue = searchParams.get('sortBy') || 'name-asc';
  const [sortField, sortDirection] = sortByValue.split('-');
  const sortModificator = sortDirection === 'asc' ? 1 : -1;

  modifiedCabins = modifiedCabins.sort(
    (a, b) => (a[sortField] - b[sortField]) * sortModificator
  );

  function renderCabinRow(cabin) {
    return <CabinRow key={cabin.id} cabin={cabin} />;
  }

  if (isFetchingCabin) return <Spinner />;

  return (
    <Menus>
      <Table columns="0.6fr 1.8fr 2.2fr 1fr 1fr 1fr">
        <Table.Header>
          <div></div>
          <div>Cabin</div>
          <div>Capacity</div>
          <div>Price</div>
          <div>Discount</div>
          <div></div>
        </Table.Header>

        <Table.Body rowsData={modifiedCabins} renderRow={renderCabinRow} />
      </Table>
    </Menus>
  );
}

export default CabinTable;
