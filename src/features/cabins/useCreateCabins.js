import { useMutation, useQueryClient } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { createCabinApi } from '../../services/apiCabins';

export function useCreateCabins() {
  const queryClient = useQueryClient();

  const { mutate: createCabin, isPending: isCreating } = useMutation({
    mutationFn: createCabinApi,
    onSuccess() {
      toast.success('Successfully create a cabin');
      queryClient.invalidateQueries({
        queryKey: ['cabins'],
      });
    },
    onError(error) {
      console.error(error);
      toast.error(error.message);
    },
  });

  return { createCabin, isCreating };
}
