import styled from 'styled-components';

import Tag from '../../ui/Tag';
import Flag from '../../ui/Flag';
import CheckInButton from './CheckInButton';
import CheckOutButton from './CheckOutButton';

const StyledTodayItem = styled.li`
  display: grid;
  grid-template-columns: 9rem 2rem 1fr 7rem 9rem;
  gap: 1.2rem;
  align-items: center;

  font-size: 1.4rem;
  padding: 0.8rem 0;
  border-bottom: 1px solid var(--color-grey-100);

  &:first-child {
    border-top: 1px solid var(--color-grey-100);
  }
`;

const Guest = styled.div`
  font-weight: 500;
`;

function TodayItem({ activity: { id, status, guests, numNights } }) {
  const isUnconfirmed = status === 'unconfirmed';
  const isCheckedIn = status === 'checked-in';

  return (
    <StyledTodayItem>
      {isUnconfirmed && <Tag $type="green">Arriving</Tag>}
      {isCheckedIn && <Tag $type="blue">Departing</Tag>}

      <Flag src={guests?.countryFlag} alt={`Flag of ${guests?.nationality}`} />
      <Guest>{guests?.fullName}</Guest>

      <span>{numNights} Nights</span>

      {isUnconfirmed && <CheckInButton bookingId={id} />}
      {isCheckedIn && <CheckOutButton bookingId={id} />}
    </StyledTodayItem>
  );
}

export default TodayItem;
