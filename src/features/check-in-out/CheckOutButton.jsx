import Button from '../../ui/Button';
import SpinnerMini from '../../ui/SpinnerMini';

import { useCheckout } from './useCheckout';

function CheckOutButton({ bookingId }) {
  const { checkout, isCheckingOut } = useCheckout();

  function click() {
    checkout(bookingId);
  }

  return (
    <Button $size="small" onClick={click}>
      {isCheckingOut ? <SpinnerMini /> : 'Check out'}
    </Button>
  );
}

export default CheckOutButton;
