import { useMutation, useQueryClient } from '@tanstack/react-query';
import toast from 'react-hot-toast';

import { updateBookingApi } from '../../services/apiBookings';

export function useCheckout() {
  const queryClient = useQueryClient();

  const { mutate: checkout, isPending: isCheckingOut } = useMutation({
    mutationFn(id) {
      return updateBookingApi(id, { status: 'checked-out' });
    },
    onSuccess(booking) {
      toast.success(`Booking #${booking.id} successfully checked out`);
      queryClient.invalidateQueries({ active: true });
    },
    onError(error) {
      toast.error('There was an error while checking out');
      console.error(error);
    },
  });

  return { checkout, isCheckingOut };
}
