import Button from '../../ui/Button';
import { useNavigate } from 'react-router-dom';

function CheckInButton({ bookingId }) {
  const navigate = useNavigate();

  function click() {
    navigate(`/checkin/${bookingId}`);
  }

  return (
    <Button $size="small" onClick={click}>
      Check in
    </Button>
  );
}

export default CheckInButton;
