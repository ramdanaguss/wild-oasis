import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import toast from 'react-hot-toast';

import { updateBookingApi } from '../../services/apiBookings';

export function useCheckin() {
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const { mutate: checkin, isPending: isCheckingIn } = useMutation({
    mutationFn({ id, breakfast = {} }) {
      return updateBookingApi(id, { status: 'checked-in', isPaid: true, ...breakfast });
    },
    onSuccess(booking) {
      toast.success(`Booking #${booking.id} successfully checked in`);
      queryClient.invalidateQueries({ active: true });
      navigate('/');
    },
    onError(error) {
      toast.error('There was an error while checking in');
      console.error(error);
    },
  });

  return { checkin, isCheckingIn };
}
