import { useEffect, useState } from 'react';
import styled from 'styled-components';

import BookingDataBox from '../../features/bookings/BookingDataBox';
import Row from '../../ui/Row';
import Heading from '../../ui/Heading';
import ButtonGroup from '../../ui/ButtonGroup';
import Button from '../../ui/Button';
import ButtonText from '../../ui/ButtonText';
import Checkbox from '../../ui/Checkbox';
import Spinner from '../../ui/Spinner';

import { useMoveBack } from '../../hooks/useMoveBack';
import { useBooking } from '../bookings/useBooking';
import { formatCurrency } from '../../utils/helper';
import { useCheckin } from './useCheckin';
import { useSettings } from '../settings/useSettings';

const Box = styled.div`
  background-color: var(--color-grey-0);
  border: 1px solid var(--color-grey-100);
  border-radius: var(--border-radius-md);
  padding: 2.4rem 4rem;
`;

function CheckinBooking() {
  const [isConfirmPaid, setIsConfirmPaid] = useState(false);
  const [isAddBreakfast, setIsAddBreakfast] = useState(false);
  const moveBack = useMoveBack();
  const { checkin, isCheckingIn } = useCheckin();
  const { booking = {}, isFetching } = useBooking();
  const { settings, isFetchingSetting } = useSettings();

  const { id, guests, totalPrice, numGuests, hasBreakfast, numNights, isPaid } = booking;

  const optionalBreakfastPrice = settings?.breakfastPrice * numGuests * numNights;

  useEffect(() => {
    if (!isPaid) return;
    setIsConfirmPaid(true);
  }, [isPaid]);

  function confirm() {
    setIsConfirmPaid((isConfirmPaid) => !isConfirmPaid);
  }

  function addBreakfast() {
    setIsAddBreakfast((isAddBreakfast) => !isAddBreakfast);
    setIsConfirmPaid(false);
  }

  function handleCheckin() {
    if (!isConfirmPaid) return;

    if (isAddBreakfast) {
      checkin({
        id,
        breakfast: {
          hasBreakfast: true,
          extrasPrice: optionalBreakfastPrice,
          totalPrice: totalPrice + optionalBreakfastPrice,
        },
      });
      return;
    }

    checkin({ id });
  }

  if (isFetching || isFetchingSetting) return <Spinner />;

  return (
    <>
      <Row $type="horizontal">
        <Heading as="h1">Check in booking #{id}</Heading>
        <ButtonText onClick={moveBack}>&larr; Back</ButtonText>
      </Row>

      <BookingDataBox booking={booking} />

      {!hasBreakfast && (
        <Box>
          <Checkbox checked={isAddBreakfast} onChange={addBreakfast}>
            Want to add breakfast for {formatCurrency(optionalBreakfastPrice)}
          </Checkbox>
        </Box>
      )}

      <Box>
        <Checkbox
          checked={isConfirmPaid}
          disabled={(isPaid && isConfirmPaid) || isCheckingIn}
          onChange={confirm}
        >
          It is confirm that {guests.fullName} has paid the total amount of{' '}
          {isAddBreakfast
            ? `${formatCurrency(totalPrice + optionalBreakfastPrice)} (${formatCurrency(
                totalPrice
              )} + ${formatCurrency(optionalBreakfastPrice)})`
            : formatCurrency(totalPrice)}
        </Checkbox>
      </Box>

      <ButtonGroup>
        <Button disabled={!isConfirmPaid || isCheckingIn} onClick={handleCheckin}>
          Check in booking #{id}
        </Button>
        <Button disabled={isCheckingIn} $variation="secondary" onClick={moveBack}>
          Back
        </Button>
      </ButtonGroup>
    </>
  );
}

export default CheckinBooking;
