import supabase, { getSupabaseImageURLApi } from './supabase';

const cabinsTable = 'cabins';
const cabinsBucket = 'cabins-images';

export async function getCabinsApi() {
  let {
    data: cabins,
    error,
    count,
  } = await supabase.from('cabins').select('*', { count: 'exact' });

  if (error) {
    throw new Error('Cannot load cabins data');
  }

  return { cabins, count };
}

export async function createCabinApi(newCabin) {
  let imageUrl = newCabin.image;
  if (typeof newCabin.image !== 'string') {
    imageUrl = await getSupabaseImageURLApi({
      imageFileList: newCabin.image,
      bucket: cabinsBucket,
    });
  }

  const { data: createdCabin, error } = await supabase
    .from(cabinsTable)
    .insert([{ ...newCabin, image: imageUrl }])
    .select()
    .single();

  if (error) {
    throw new Error('Cannot create cabin');
  }

  return createdCabin;
}

export async function updateCabinApi(cabin, cabinId) {
  let imageUrl = cabin.image;
  if (typeof cabin.image !== 'string') {
    imageUrl = await getSupabaseImageURLApi({
      imageFileList: cabin.image,
      bucket: cabinsBucket,
    });
  }

  const { error, data: updatedCabin } = await supabase
    .from(cabinsTable)
    .update({ ...cabin, image: imageUrl })
    .eq('id', cabinId)
    .select();

  if (error) {
    throw new Error('Cannot update cabin');
  }

  return updatedCabin;
}

export async function deleteCabinApi(id) {
  const { error } = await supabase.from('cabins').delete().eq('id', id);

  if (error) {
    throw new Error('Cannot delete cabin data');
  }
}
