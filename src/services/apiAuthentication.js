import supabase, { getSupabaseImageURLApi } from './supabase';

export async function loginApi(email, password) {
  const { data, error } = await supabase.auth.signInWithPassword({
    email,
    password,
  });

  if (error) {
    throw new Error('Cannot login the user');
  }

  return data;
}

export async function logoutApi() {
  const { error } = await supabase.auth.signOut();
  if (error) throw new Error('Cannot logout the user');
}

export async function signUpApi({ email, password, fullName }) {
  const { data, error } = await supabase.auth.signUp({
    email,
    password,
    options: {
      data: {
        fullName,
        avatar: '',
      },
    },
  });

  if (error) {
    throw new Error('Cannot create new user');
  }

  return data;
}

export async function getUserApi() {
  const { data: session, error: sessionError } = await supabase.auth.getSession();

  if (sessionError) {
    throw new Error('Cannot retrieving the session');
  }

  if (!session) return null;

  const {
    data: { user },
    error: getUserError,
  } = await supabase.auth.getUser();

  if (getUserError) {
    throw new Error('Cannot retrieving user data');
  }

  return user;
}

export async function updateUserDataApi({ fullName, avatar }) {
  const updatedUserData = {
    data: {
      fullName,
      avatar,
    },
  };

  if (typeof avatar !== 'string') {
    updatedUserData.data.avatar = await getSupabaseImageURLApi({
      imageFileList: avatar,
      bucket: 'avatars',
    });
  }

  const { data: userData, error } = await supabase.auth.updateUser(updatedUserData);

  if (error) throw new Error('Cannot update user data');

  return userData;
}

export async function updateUserPasswordApi(password) {
  const { data: userData, error } = await supabase.auth.updateUser({
    password,
  });

  if (error) throw new Error('Cannot update user password');

  return userData;
}
