import { getToday } from '../utils/helper';
import supabase from './supabase';

export async function getBookingsApi({ filter, sortBy, pageOption }) {
  let query = supabase
    .from('bookings')
    .select(
      'id, created_at, startDate, endDate, numNights, numGuests, status, totalPrice, cabins(name), guests(fullName, email)',
      { count: 'exact' }
    );

  if (filter) query = query[filter.method](filter.field, filter.value);

  if (sortBy)
    query = query.order(sortBy.field, { ascending: sortBy.direction === 'asc' });

  if (pageOption) {
    query = query.range(pageOption.from, pageOption.to);
  }

  const { data: bookings, error, count } = await query;

  if (error) {
    throw new Error('Cannot load bookings data');
  }

  return { bookings, count };
}

export async function getBookingApi(id) {
  const { data, error } = await supabase
    .from('bookings')
    .select('*, cabins(*), guests(*)')
    .eq('id', id)
    .single();

  if (error) {
    throw new Error('Booking not found');
  }

  return data;
}

export async function getBookingsAfterDateApi(date) {
  const { data: bookings, error } = await supabase
    .from('bookings')
    .select('created_at, totalPrice, extrasPrice, isPaid')
    .gte('created_at', date)
    .lte('created_at', getToday({ end: true }));

  if (error) {
    throw new Error('Cannot load bookings data');
  }

  return bookings;
}

export async function getStaysAfterDateApi(date) {
  const { data: bookings, error } = await supabase
    .from('bookings')
    .select('*, guests(fullName)')
    .gte('startDate', date)
    .lte('startDate', getToday({ end: true }));

  if (error) {
    throw new Error('Cannot load bookings data');
  }

  return bookings;
}

export async function updateBookingApi(id, obj) {
  const { data: booking, error } = await supabase
    .from('bookings')
    .update(obj)
    .eq('id', id)
    .select()
    .single();

  if (error) {
    throw new Error('Booking could not be updated');
  }

  return booking;
}

export async function deleteBookingApi(id) {
  const { error } = await supabase.from('bookings').delete().eq('id', id);

  if (error) {
    throw new Error('Cannot delete booking data');
  }
}

export async function getStaysTodayActivity() {
  const { data: activities, error } = await supabase
    .from('bookings')
    .select('*, guests(fullName, nationality, countryFlag)')
    .or(
      `and(status.eq.unconfirmed,startDate.eq.${getToday()}),and(status.eq.checked-in,endDate.eq.${getToday()})`
    )
    .order('created_at');

  if (error) {
    throw new Error('Cannot retrieve today activities');
  }

  return activities;
}
