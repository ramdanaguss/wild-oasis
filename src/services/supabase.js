import { createClient } from '@supabase/supabase-js';

export const supabaseUrl = import.meta.env.VITE_SUPABASE_URL;
const supabaseKey = import.meta.env.VITE_SUPABASE_KEY;
const supabase = createClient(supabaseUrl, supabaseKey);

export async function getSupabaseImageURLApi({ imageFileList, bucket }) {
  const imageFile = imageFileList[0];

  const imageName = `${Math.random()}-${imageFile.name}`;

  // Upload image
  const { error } = await supabase.storage.from(bucket).upload(imageName, imageFile);

  if (error) {
    throw new Error('Cannot upload an image');
  }

  // Retrieve image url
  const {
    data: { publicUrl: imageUrl },
  } = supabase.storage.from(bucket).getPublicUrl(imageName);

  if (!imageUrl) {
    throw new Error('Cannot generate image url');
  }

  return imageUrl;
}

export default supabase;
