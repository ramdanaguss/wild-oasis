import { useSearchParams } from 'react-router-dom';
import Select from './Select';

function SortBy({ options, type }) {
  const [searchParams, setSearchParams] = useSearchParams();

  const currentValue = searchParams.get('sortBy') || '';

  function change(event) {
    searchParams.set('sortBy', event.target.value);
    setSearchParams(searchParams);
  }

  return <Select options={options} value={currentValue} onChange={change} type={type} />;
}

export default SortBy;
