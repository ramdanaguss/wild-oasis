import { formatDistance, parseISO } from 'date-fns';

export const formatCurrency = (value) =>
  new Intl.NumberFormat('id', { style: 'currency', currency: 'IDR' }).format(value);

export const formatDistanceFromNow = (dateStr) =>
  formatDistance(parseISO(dateStr), new Date(), {
    addSuffix: true,
  })
    .replace('about ', '')
    .replace('in', 'In');

export function getToday(option = { end: false }) {
  const today = new Date();

  if (option.end) {
    today.setUTCHours(23, 59, 59, 999);
  } else {
    today.setUTCHours(0, 0, 0, 0);
  }

  return today.toISOString();
}
