import { useSearchParams } from 'react-router-dom';

export function usePagination(count = 0) {
  const [searchParams, setSearchParams] = useSearchParams();

  const page = +searchParams.get('page') || 1;
  const nextPage = page + 1;
  const prevPage = page - 1;

  const pageSize = +import.meta.env.VITE_PAGE_SIZE;
  const totalPage = Math.ceil(count / pageSize);
  const isFirstPage = page <= 1;
  const isLastPage = page >= totalPage;

  // *ForAPI is calculated fto become zero based, otherwise it is start from 1
  const fromUI = (page - 1) * pageSize + 1;
  const toUI = isLastPage ? count : fromUI + pageSize - 1;
  const fromAPI = fromUI - 1;
  const toAPI = fromAPI + pageSize - 1;

  // For next page
  const nextFromAPI = isLastPage ? fromAPI : (nextPage - 1) * pageSize;
  const nextToAPI = isLastPage ? toAPI : nextFromAPI + pageSize - 1;

  // For previous page
  const prevFromAPI = isFirstPage ? fromAPI : (prevPage - 1) * pageSize;
  const prevToAPI = isFirstPage ? toAPI : prevFromAPI + pageSize - 1;

  function next() {
    const nextPage = isLastPage ? page : page + 1;

    searchParams.set('page', nextPage);
    setSearchParams(searchParams);
  }

  function previous() {
    const previousPage = isFirstPage ? 1 : page - 1;

    searchParams.set('page', previousPage);
    setSearchParams(searchParams);
  }

  return {
    page,
    nextPage,
    prevPage,
    totalPage,
    fromUI,
    toUI,
    fromAPI,
    toAPI,
    nextFromAPI,
    nextToAPI,
    prevFromAPI,
    prevToAPI,
    isFirstPage,
    isLastPage,
    previous,
    next,
  };
}
