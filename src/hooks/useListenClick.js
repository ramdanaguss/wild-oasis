import { useEffect, useRef } from 'react';

export function useListenClick(handler, listenToCapture = true) {
  const elementRef = useRef(null);

  useEffect(() => {
    if (!elementRef.current) return;

    const clickHandler = (event) => {
      if (!elementRef.current || elementRef.current?.contains(event.target)) return;
      handler();
    };

    document.addEventListener('click', clickHandler, listenToCapture);

    return () => {
      document.removeEventListener('click', clickHandler);
    };
  }, [handler, listenToCapture]);

  return { elementRef };
}
